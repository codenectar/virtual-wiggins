# Virtual Wiggins

See [Raspberry Pi Documentation - Getting started](https://www.raspberrypi.com/documentation/computers/getting-started.html)

## Getting Started

The following assumes that you are doing development with a Mac.

* Obtain hardware:
  * Raspberry Pi Zero
  * A microSD card (and any adaptors needed to connect it to your computer)
* Obtain software:
  * Get the [Raspberry Pi Imager](https://downloads.raspberrypi.org/imager/imager_latest.dmg)
  * Download [Raspberry Pi OS Lite (Legacy) WITHOUT desktop](https://www.raspberrypi.com/software/operating-systems/#raspberry-pi-os-legacy)
  * Run `host$ shasum -a256` on the downloaded file to check the SHA256 file integrity hash against what is given on the website

## Create Initial Bootdisk

* Plug the microSD card into your computer
* Run the imager:
  * For Operating System, choose *Use custom* and then the .xz file you downloaded
  * For Storage, choose the microSD card you plugged in
  * Click on the gear icon to get into Settings:
    * When asked whether to prefill the wifi password from the system keychain, answer Yes (or enter other settings later)
    * Choose "for this session only" as image customization option
    * Set hostname (fine to use default of raspberrypi.local if you like)
    * Enable SSH
      * Another shortcut is to set "Allow public-key authentication only" and it will use your $HOME/.ssh/id_rsa.pub key
    * Set username and password (even though password should not be needed to log in by SSH)
    * Configure the WiFi if you didn't prefill from system keychain
      * Make sure it is the same network that your development machine is on (e.g. not a 5G version)
      * Wireless LAN country: set to your country (?)
    * Set locale settings
    * Save
  * Write
* Unplug the microSD card which now is ready to boot.  But before you do that...
* Do a ping scan of your local network to see which hosts are aleady online.  Here is an example:
```
host$ nmap -sn 192.168.18.1/24
Starting Nmap 7.94 ( https://nmap.org ) at 2023-07-25 13:20 CEST
Nmap scan report for 192.168.18.1
Host is up (0.0098s latency).
Nmap scan report for 192.168.18.6
Host is up (0.0081s latency).
Nmap scan report for 192.168.18.17
Host is up (0.00028s latency).
Nmap scan report for 192.168.18.47
Host is up (0.0048s latency).
Nmap scan report for 192.168.18.60
Host is up (0.078s latency).
Nmap done: 256 IP addresses (5 hosts up) scanned in 13.27 seconds
```

## First Boot

* Plug the boot disk into your Raspberry Pi Zero
* Connect the Raspberry Pi to a USB socket to give it power
* Watch the nice little green light blink for a minute or two
* Repeat the ping scan from above until you see a new host appear on the local network.  For example:
```
host$ nmap -sn 192.168.18.1/24
Starting Nmap 7.94 ( https://nmap.org ) at 2023-07-25 14:02 CEST
Nmap scan report for 192.168.18.1
Host is up (0.0057s latency).
Nmap scan report for 192.168.18.6
Host is up (0.0049s latency).
Nmap scan report for 192.168.18.17
Host is up (0.00021s latency).
Nmap scan report for 192.168.18.47
Host is up (0.012s latency).
Nmap scan report for 192.168.18.60
Host is up (0.055s latency).
Nmap scan report for 192.168.18.107
Host is up (0.015s latency).
Nmap done: 256 IP addresses (6 hosts up) scanned in 21.68 seconds
```
* Log in to your new machine:
  * In the example above, the new IP address 192.168.18.107 has appeared, so
    `host$ ssh -o "StrictHostKeyChecking no" 192.168.18.107`
* Update the software packages
```
pi$ sudo apt-get update && sudo apt-get upgrade
```

## Attach Camera

* Remove the ribbon cable from the RasPi Camera v2 module
* Insert the short adaptor ribbon cable into the RasPi camera module
* Plug the other end into the RasPi Zero

## Enable Camera

* After RasPi boots, log in again, and run configuration shell:
```
pi$ sudo raspi-config
```
* Select `3 Interface Options`
  * Select `P1 Camera`
    * Select `Yes` when asked whether to enable the camera interface
```
pi$ sudo reboot
```
* After RasPi boots, log in again, and take a snap:
```
pi$ raspistill -o first-image.jpg
```

## Attach Codec Zero Hat

* Edit `/boot/config.txt` to make this so:
```
#dtparam=audio=on
dtoverlay=iqaudio-codec
```

## Put some MP3 music on the Pi Zero

* Copy your favorite MP3 track from Host to Pi.  For example:
```
host$ $ scp ~/Music/MP3/Mentors_mp3s/06_TwoPeasInAPod.mp3 192.168.18.107:
```
* Install music player on the Pi Zero:
```
pi$ sudo apt-get update
pi$ sudo apt-get install mpg123
```
It suggests other packages: nas jackd oss-compat oss4-base pulseaudio
* Run a speaker test:
```
pi$ speaker-test
```
Nothing.  Debug...
```
pi$ aplay -l
**** List of PLAYBACK Hardware Devices ****
card 0: IQaudIOCODEC [IQaudIOCODEC], device 0: IQaudIO CODEC HiFi v1.2 da7213-hifi-0 [IQaudIO CODEC HiFi v1.2 da7213-hifi-0]
  Subdevices: 1/1
  Subdevice #0: subdevice #0
$ dmesg | grep da7213
[   29.522282] da7213 1-001a: supply VDDA not found, using dummy regulator
[   29.570162] da7213 1-001a: supply VDDIO not found, using dummy regulator
[   29.571609] da7213 1-001a: supply VDDMIC not found, using dummy regulator
```
Try this:
```
host$ git clone https://github.com/iqaudio/Pi-Codec.git
host$ cd Pi-Codec
host$ scp Codec_Zero_* 192.168.18.107:
```
...and then on the Pi Zero...
```
pi$ sudo alsactl restore -f Codec_Zero_Playback_only.state
pi$ speaker-test -t wav -c 1
```
*YAY!!* I hear a repeating "Front Left" spoken by a nice voice!
...and I can play my sound file with:
```
pi$ mpg123 06_TwoPeasInAPod.mp3
```
*w00t w00t*

Further info: using `alsamix` it appears that the only mixer controls affecting this playback are *LineOut* and *DAC*.

## Make a Test Recording

Load a different ALSA state file from that repo, and make a five second test recording:
```
pi$ alsactl restore -f Codec_Zero_OnboardMIC_record_and_SPK_playback.state
pi$ arecord -d 5 -f cd foo.cd
```

Now reload the playback ALSA state file, and play back what you recorded:
```
pi$ alsactl restore -f Codec_Zero_Playback_only.state
pi$ aplay -f cd foo.cd
```
*w00t w00t*

## Load sound settings on boot

Save the current state of the mixer:
```
pi$ sudo alsactl store -f /root/alsa.state
```

Edit `/etc/rc.local` to add this line to load the sound state file:
```
sudo alsactl restore -f /root/alsa.state
```

## ETC

For some info about ALSA controls, try:

```
pi$ alsactl --help
```

For a list of audio capture devices:
```
$ arecord -l
**** List of CAPTURE Hardware Devices ****
card 0: IQaudIOCODEC [IQaudIOCODEC], device 0: IQaudIO CODEC HiFi v1.2 da7213-hifi-0 [IQaudIO CODEC HiFi v1.2 da7213-hifi-0]
  Subdevices: 1/1
  Subdevice #0: subdevice #0
```

