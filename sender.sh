#!/usr/bin/env bash

WIDTH=640
HEIGHT=480
HOST_IP='192.168.18.17'
HOST_PORT=9000

BITRATE=1700000

#raspivid --framerate 30 \
#         --width $WIDTH \
#         --height $HEIGHT \
#         --exposure auto \
#         --awb auto \
#         --inline \
#         --timeout 0 \
#         --output - | \
#gst-launch-1.0 -v \
#    fdsrc ! \
#    "video/x-h264,width=$WIDTH,height=$HEIGHT" ! \
#    h264parse ! \
#    rtph264pay pt=96 ! \
#    udpsink host=$HOST_IP port=$HOST_PORT

raspivid --framerate 30 \
         --width $WIDTH \
         --height $HEIGHT \
         --exposure auto \
         --awb auto \
         --inline \
         --timeout 0 \
         --output - | \
gst-launch-1.0 -v \
    fdsrc ! \
    h264parse ! \
    rtph264pay ! \
    udpsink host=$HOST_IP port=$HOST_PORT
